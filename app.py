import numpy as np
import pandas as pd
import pickle
import webbrowser
from flask import Flask, request, jsonify, render_template
from waitress import serve

app = Flask(__name__)

# read and prepare model 
model = pickle.load(open('model.pkl', 'rb'))
webbrowser.open('http://localhost:8080/', new=2)
@app.route('/')
def home():
    return render_template('./test.html')

@app.route('/predict', methods=['POST'])
def predict():
    ''' 
    Rendering results on HTML
    '''
    # get data
    features = dict(request.form)   
    
    # expected keys
    keys = ['Lufttemperatur', 'Vindstyrke', 'Solskinstid', 'Vindkast','Globalstraling','Lufttrykk','Time','Dag','Måned','Year','Ukedag','Helg']
    
    # prepare for prediction
    
    features_df = pd.DataFrame(features, index=[0]).loc[:,keys]
    print(features_df)
    


    # predict
    prediction = model.predict(features_df)
    prediction = np.round(prediction[0])
    prediction = np.clip(prediction, 0, np.inf)

    # prepare output
    return render_template('./test.html',
                           prediction_text='Forventet antall sykler {}'.format(prediction))

if __name__ == '__main__':
    serve(app, host='0.0.0.0', port=8080)